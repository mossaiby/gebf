// Includes

#include <cmath>

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>

#include <viennacl/matrix.hpp>
#include <viennacl/vector.hpp>

#include <boost/numeric/bindings/lapack/driver/gesdd.hpp>
#include <boost/numeric/bindings/lapack/driver/gelsd.hpp>
#include <boost/numeric/bindings/blas/level3/gemm.hpp>

#include <boost/numeric/bindings/ublas/matrix.hpp>
#include <boost/numeric/bindings/ublas/vector.hpp>
#include <boost/numeric/bindings/std/vector.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>

#include <boost/numeric/ublas/io.hpp>

#include <viennacl/tools/timer.hpp>

#include "utils.h"

#define JOBNAME "test"

namespace ublas = boost::numeric::ublas;

namespace blas = boost::numeric::bindings::blas;
namespace lapack = boost::numeric::bindings::lapack;

int main()
{
    std::cout << "C++, DGESDD";

    #ifdef _OPENMP

    std::cout << ", OpenMP enabled";

    #endif

    std::cout << std::endl;

    // Reading data

    int NodeNo;
    int BNodeNo;

    int ModeDataNo;
    int ModeNo;

    ublas::vector <int> ControlData;
    ublas::vector <double> ControlData2;

    ublas::matrix <double> ModeData;

    ublas::matrix <double> Nodes;
    ublas::vector <int> BNodes;

    ublas::vector <int> BCTypes;
    ublas::vector <double> BCValues;

    ublas::vector <double> DCValues;

    viennacl::tools::timer timer;

    timer.start();

    readVectorFromFile("../../data3d/" JOBNAME ".data", ControlData, 3);
    readVectorFromFile("../../data3d/" JOBNAME ".data2", ControlData2, 1);

    NodeNo = ControlData[0];
    BNodeNo = ControlData[1];
    ModeDataNo = ControlData[2];

    double k = ControlData2[0];

    ModeNo = 2 * ModeDataNo * ModeDataNo * ModeDataNo * ModeDataNo * ModeDataNo * ModeDataNo;

    readMatrixFromFile("../../data3d/" JOBNAME ".modes", ModeData, ModeNo / 2, 6);

    readMatrixFromFile("../../data3d/" JOBNAME ".nodes", Nodes, NodeNo, 3);
    readVectorFromFile("../../data3d/" JOBNAME ".bnodes", BNodes, BNodeNo);

    readVectorFromFile("../../data3d/" JOBNAME ".bctypes", BCTypes, BNodeNo);
    readVectorFromFile("../../data3d/" JOBNAME ".bcvalues", BCValues, BNodeNo);

    readVectorFromFile("../../data3d/" JOBNAME ".dcvalues", DCValues, NodeNo);

    std::cout << "Reading data took " << timer.get() << " seconds." << std::endl;

    std::cout << ModeNo << " modes, " << NodeNo << " domain nodes, " << BNodeNo << " boundary nodes." << std::endl;

    // Starting solution
    ublas::matrix <double, ublas::column_major> Q(NodeNo, ModeNo);

    timer.start();

    #pragma omp parallel for
    for (int i = 0; i < NodeNo; i++)
    {
        for (int j = 0; j < ModeNo / 2; j++)
        {
            double x = Nodes(i, 0);
            double y = Nodes(i, 1);
            double z = Nodes(i, 2);

            double a = ModeData(j, 0);
            double b = ModeData(j, 1);
            double c = ModeData(j, 2);
            double d = ModeData(j, 3);
            double e = ModeData(j, 4);
            double f = ModeData(j, 5);

            Q(i, 2 * j + 0) = std::exp(a * x + b * y + c * z) * ((a * a + b * b + c * c - d * d - e * e - f * f + k * k) * std::cos(d * x + e * y + f * z) - 2 * (a * d + b * e + c * f) * std::sin(d * x + e * y + f * z));
            Q(i, 2 * j + 1) = std::exp(a * x + b * y + c * z) * ((a * a + b * b + c * c - d * d - e * e - f * f + k * k) * std::sin(d * x + e * y + f * z) + 2 * (a * d + b * e + c * f) * std::cos(d * x + e * y + f * z));
        }
    }

    std::cout << "Building Q matrix took " << timer.get() << " seconds." << std::endl;

    int MinMN = std::min(NodeNo, ModeNo);

    ublas::matrix <double, ublas::column_major> U(NodeNo, NodeNo);
    ublas::matrix <double, ublas::column_major> VT(ModeNo, ModeNo);
    ublas::vector <double> S(MinMN);

    timer.start();

    lapack::gesdd('A', Q, S, U, VT);

    std::cout << "SVD took " << timer.get() << " seconds." << std::endl;

    const double Eps1 = std::numeric_limits <double> ::epsilon() * 100.00;
    const double Eps2 = Eps1 * S[0];

    // Determine matrix rank
    int r = 0;

    for (int i = 0; i < MinMN; i++)
    {
        if (S[i] > Eps2)
        {
            r++;
        }
    }

    int b = ModeNo - r;

    std::cout << "Rank(Q) = " << r << ", no. of null space bases: " << b << std::endl;

    // Null space of Q

    ublas::matrix <double, ublas::column_major> T(b, ModeNo);  // TODO: Column major?

    for (int i = 0; i < b; i++)
    {
        for (int j = 0; j < ModeNo; j++)
        {
            T(i, j) = VT(i + r, j);
        }
    }

    // Elements of pseudo-inverse of Q

    ublas::matrix <double, ublas::column_major> UTR(r, NodeNo);
    ublas::matrix <double, ublas::column_major> VR(ModeNo, r);

    timer.start();

    for (int i = 0; i < r; i++)
    {
        for (int j = 0; j < NodeNo; j++)
        {
            UTR(i, j) = U(j, i) / S[i];
        }
    }

    for (int i = 0; i < ModeNo; i++)
    {
        for (int j = 0; j < r; j++)
        {
            VR(i, j) = VT(j, i);
        }
    }

    ublas::vector <double> TC;
    ublas::vector <double> CP;

    TC = prod(UTR, DCValues);
    CP = prod(VR, TC);

    std::cout << "Calculating CP took " << timer.get() << " seconds." << std::endl;

    ublas::matrix <double, ublas::column_major> V(BNodeNo, ModeNo);  // TODO: Column major?

    timer.start();

    for (int i = 0; i < BNodeNo; i++)
    {
        for (int j = 0; j < ModeNo / 2; j++)
        {
            double x = Nodes(BNodes[i], 0);
            double y = Nodes(BNodes[i], 1);
            double z = Nodes(BNodes[i], 2);

            double a = ModeData(j, 0);
            double b = ModeData(j, 1);
            double c = ModeData(j, 2);
            double d = ModeData(j, 3);
            double e = ModeData(j, 4);
            double f = ModeData(j, 5);

            V(i, 2 * j + 0) = std::exp(a * x + b * y + c * z) * std::cos(d * x + e * y + f * z);
            V(i, 2 * j + 1) = std::exp(a * x + b * y + c * z) * std::sin(d * x + e * y + f * z);
        }
    }

    std::cout << "Calculating V took " << timer.get() << " seconds." << std::endl;

    ublas::matrix <double, ublas::column_major> K(BNodeNo, b);  // TODO: Column major?

    timer.start();

    blas::gemm(1.00, V, trans(T), 0.00, K);

    std::cout << "Calculating K took " << timer.get() << " seconds." << std::endl;

    ublas::matrix <double, ublas::column_major> Psi(NodeNo, ModeNo);  // TODO: Column major?

    timer.start();

    for (int i = 0; i < NodeNo; i++)
    {
        for (int j = 0; j < ModeNo / 2; j++)
        {
            double x = Nodes(i, 0);
            double y = Nodes(i, 1);
            double z = Nodes(i, 2);

            double a = ModeData(j, 0);
            double b = ModeData(j, 1);
            double c = ModeData(j, 2);
            double d = ModeData(j, 3);
            double e = ModeData(j, 4);
            double f = ModeData(j, 5);

            Psi(i, 2 * j + 0) = std::exp(a * x + b * y + c * z) * std::cos(d * x + e * y + f * z);
            Psi(i, 2 * j + 1) = std::exp(a * x + b * y + c * z) * std::sin(d * x + e * y + f * z);
        }
    }

    std::cout << "Calculating Psi took " << timer.get() << " seconds." << std::endl;

    ublas::vector <double> RHS(BNodeNo);

    timer.start();

    RHS = BCValues - prod(V, CP);

    std::cout << "Calculating RHS took " << timer.get() << " seconds." << std::endl;

    int MaxMN = std::max(BNodeNo, b);

    ublas::matrix <double, ublas::column_major> B(MaxMN, 1);  // TODO: Column major?

    for (int i = 0; i < BNodeNo; i++)
    {
        B(i, 0) = RHS[i];
    }

    int Rank;

    timer.start();

    lapack::gelsd(K, B, S, Eps1, Rank);

    std::cout << "Calculating CH took " << timer.get() << " seconds." << std::endl;

    ublas::vector <double> TTd(ModeNo);

    ublas::vector <double> d(b);
    ublas::vector <double> Ucd;

    for (int i = 0; i < b; i++)
    {
        d[i] = B(i, 0);
    }

    timer.start();

    TTd = prod(trans(T), d);
    Ucd = prod(Psi, TTd);

    std::cout << "Calculating final results took " << timer.get() << " seconds." << std::endl;

    writeVectorToFile("../../data3d/" JOBNAME ".ucd", Ucd);

    return 0;
}
