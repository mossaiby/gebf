//
// readVectorFromFile
// Reads a vector from file, no size specified

template <typename VectorType> bool readVectorFromFile(const std::string &filename, VectorType &vec)
{
    typedef typename viennacl::result_of::value_type<VectorType>::type ScalarType;

    std::ifstream file(filename.c_str());

    if (!file)
    {
        return false;
    }

    vec.clear();

    ScalarType element;
    while (file >> element)
    {
        vec.push_back(element);
    }

    return true;
}

//
// readVectorFromFile
// Reads a vector from file, size specified

template <typename VectorType> bool readVectorFromFile(const std::string &filename, VectorType &vec, int size)
{
    typedef typename viennacl::result_of::value_type<VectorType>::type ScalarType;

    std::ifstream file(filename.c_str());

    if (!file)
    {
        return false;
    }

    viennacl::traits::resize(vec, size);

    for (int i = 0; i < size; i++)
    {
        ScalarType element;
        file >> element;
        vec[i] = element;
    }

    return true;
}

//
// readMatrixFromFile
// Reads a matrix from file, sizes specified

template <typename MatrixType> bool readMatrixFromFile(const std::string &filename, MatrixType &mat, int size1, int size2)
{
    typedef typename viennacl::result_of::value_type<MatrixType>::type ScalarType;

    std::ifstream file(filename.c_str());

    if (!file)
    {
        return false;
    }

    viennacl::traits::resize(mat, size1, size2);

    for (int i = 0; i < size1; i++)
    {
        for (int j = 0; j < size2; j++)
        {
            ScalarType element;
            file >> element;
            mat(i, j) = element;
        }
    }

    return true;
}

//
// writeVectorToFile
// Writes a vector to file

template <typename VectorType> bool writeVectorToFile(const std::string &filename, const VectorType &vec)
{
    typedef typename viennacl::result_of::value_type<VectorType>::type ScalarType;

    std::ofstream file(filename.c_str());

    if (!file)
    {
        return false;
    }

    file.precision(16);

    for (unsigned int i = 0; i < vec.size(); i++)
    {
        ScalarType element;
        element = vec[i];
        file << element << std::endl;
    }

    return true;
}

//
// writeMatrixToFile
// Writes a matrix to file

template <typename MatrixType> bool writeMatrixToFile(const std::string &filename, const MatrixType &mat)
{
    typedef typename viennacl::result_of::value_type<MatrixType>::type ScalarType;

    std::ofstream file(filename.c_str());

    if (!file)
    {
        return false;
    }

    file.precision(16);

    for (unsigned int i = 0; i < mat.size1(); i++)
    {
        for (unsigned int j = 0; j < mat.size2(); j++)
        {
            ScalarType element;
            element = mat(i, j);
            file << element << " ";
        }

        file << std::endl;
    }

    return true;
}
